# HD Forge for OpenMW-Lua

Automatically replace vanilla forge bellows with animated bellows with sound. Compatible with everything.

**Requires OpenMW 0.49 or newer and [HD Forge](https://www.nexusmods.com/morrowind/mods/46738)!**

#### Credits

**[HD Forge](https://www.nexusmods.com/morrowind/mods/46738) was created by** Pherim

**OpenMW-Lua script by:**

* johnnyhostile
* zackhasacat
* ezze
* Ronik

#### Web

[Project Home](https://modding-openmw.gitlab.io/hd-forge-for-openmw-lua/)

<!-- [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->

[Source on GitLab](https://gitlab.com/modding-openmw/hd-forge-for-for-openmw-lua)

#### Installation

**Requires OpenMW 0.49 or newer and [HD Forge](https://www.nexusmods.com/morrowind/mods/46738)!**

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/hd-forge-for-openmw-lua/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\ObjectsClutter\hd-forge-for-openmw-lua

        # Linux
        /home/username/games/OpenMWMods/ObjectsClutter/hd-forge-for-openmw-lua

        # macOS
        /Users/username/games/OpenMWMods/ObjectsClutter/hd-forge-for-openmw-lua

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\ObjectsClutter\hd-forge-for-openmw-lua"`)
1. Add `content=PherimAnimatedBellowsSounds.omwaddon` and `content=HD Forge.omwscripts` to your load order in `openmw.cfg` or enable it via OpenMW-Launcher

Please also note that the ESP from the original [HD Forge](https://www.nexusmods.com/morrowind/mods/46738) by Pherim is not required when using this. If you have it loaded a warning will be printed to the log/console.

#### Known Issues

* Lua-created objects do not have distant statics ([OpenMW bug link](https://gitlab.com/OpenMW/openmw/-/issues/7452))

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/hd-forge-for-openmw-lua/-/issues)
* Email `hd-forge-for-openmw-lua@modding-openmw.com`
* Contact the author on Discord: `johnnyhostile#6749`
* Contact the author on Libera.chat IRC: `johnnyhostile`
