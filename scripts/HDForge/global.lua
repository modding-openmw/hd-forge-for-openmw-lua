local core = require('openmw.core')
local vfs = require('openmw.vfs')

if core.API_REVISION < 39 then
    error("This requires OpenMW 0.49 or newer! https://openmw.org/downloads/")
end

if not vfs.fileExists("sound\\fx\\Bellows1.wav") then
    error("Missing assets from HD Forge!")
end

if not core.contentFiles.has("PherimAnimatedBellowsSounds.omwaddon") then
    error("Missing PherimAnimatedBellowsSounds.omwaddon!")
end

if core.contentFiles.has("Pherim Animated Bellows Sounds.esp") then
    print()
    print()
    print("          !!!! WARNING !!!!")
    print("The 'Pherim Animated Bellows Sounds.esp' plugin was detected in your load order -- it is not required when you have this loaded, please disable it!")
    print("          !!!! WARNING !!!!")
    print()
    print()
end

local types = require('openmw.types')
local v3 = require('openmw.util').vector3
local world = require('openmw.world')

local srcId = "furn_de_bellows_01"
local destId = "furn_de_bellows_01a"
local minAngle = 25
local maxAngle = 360 - 25 -- 335

local function replace(obj)
    if not obj.type ~= types.Static and obj.recordId ~= srcId then
        -- This isn't the right type or recordId
        return false
    end
    local angle = math.deg(math.abs(obj.position.y))
	return not ((angle > minAngle) and (angle < maxAngle))
end

local function onObjectActive(obj)
    if replace(obj) == true then
        local ab = world.createObject(destId)
        ab.enabled = obj.enabled
        ab:setScale(obj.scale)
        ab:teleport(obj.cell, obj.position + obj.rotation * v3(0, 0, 0), obj.rotation)
        obj:remove()
    end
end

return { engineHandlers = { onObjectActive = onObjectActive } }
