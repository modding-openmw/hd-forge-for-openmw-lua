## HD Forge for OpenMW-Lua Changelog

#### Version 1.2

* Optimization: the mod now filters unwanted object IDs before doing math-related things

[Download Link](https://gitlab.com/modding-openmw/hd-forge-for-openmw-lua/-/packages/30573299)

#### Version 1.1

* Scale is now preserved when replacing
* The mod returns an error when assets from HD Forge are not found

[Download Link](https://gitlab.com/modding-openmw/hd-forge-for-openmw-lua/-/packages/23716399)

#### Version 1.0

Initial version of the mod.

[Download Link](https://gitlab.com/modding-openmw/hd-forge-for-openmw-lua/-/packages/23611097)
